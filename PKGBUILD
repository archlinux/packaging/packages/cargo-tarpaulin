# Maintainer: Orhun Parmaksız <orhun@archlinux.org>
# Contributor: Maxime Gauduin <alucryd@archlinux.org>

pkgname=cargo-tarpaulin
pkgver=0.32.2
pkgrel=1
pkgdesc="Tool to analyse test coverage of cargo projects"
arch=('x86_64')
url=https://github.com/xd009642/tarpaulin
license=('Apache-2.0' 'MIT')
depends=(
  'cargo'
  'gcc-libs'
  'glibc'
  'libcurl.so'
  'openssl'
  'libssh2'
  'libgit2'
  'zlib'
)
makedepends=('git')
source=("$pkgname-$pkgver::git+$url.git#tag=$pkgver")
b2sums=('7a93ae13f48f0ec6571061f3259f04b531d261515406a6bd8d7cc38f89f5399f21976e7a961b945220603bbc62424fce1209a7f1938285b8ce5736b8c364d865')

prepare() {
  cd "$pkgname-$pkgver"
  cargo fetch --locked --target "$(rustc -vV | sed -n 's/host: //p')"
}

build() {
  cd "$pkgname-$pkgver"
  export LIBGIT2_NO_VENDOR=1
  export LIBSSH2_SYS_USE_PKG_CONFIG=1
  CFLAGS+=" -ffat-lto-objects"
  cargo build --release --frozen 
}

package() {
  cd "$pkgname-$pkgver"
  install -Dm 755 "target/release/${pkgname}" -t "${pkgdir}"/usr/bin/
  install -Dm 644 README.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 LICENSE-MIT -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim: ts=2 sw=2 et:
